# Where Should I Live? #

**Where Should I Live?** aims at helping you find the best place to live in a city, based on what you value most. 
Whether you wanna be close to a bar, don't have a car or want to live in a quiet area, **Where Should I Live?** helps you find the place that will fill most of your preferences in a minimum amount of time.

A demo of the latest version of **Where Should I Live?** can be found [here](http://spo2tu.be:8002/).

## Technical Stuff ##

The current project is *fully client side*. This means that you do not need to install anything to run the app.
However, you do need to have some kind of way to serve the static elements. 

The simplest I can recommend is to use the awesome python SimpleHttpServer module. 
Just place yourself in the work folder and run

```
#!shell

$ python -m SimpleHTTPServer
```

### Getting Started in 1 minute ###

```
#!shell
$ apt-get install python git
$ git clone https://jlengrand@bitbucket.org/jlengrand/whereshouldilive.git
$ cd whereshouldilive
$ python -m SimpleHTTPServer
```


In a nutshell, the app is built with [backbone](http://backbonejs.org/), [leaflet](http://leafletjs.com/) and uses [requirejs](http://www.requirejs.org/) for import management. 


### Dependencies ###

The project uses [bower] to handle dependencies. 
You can still work with the project things without having bower installed yourself though. 

* If you are just using code, you do not need bower.
* If you are implementing something 
    * If you know about bower, feel free to install any new dependency
        * Please think about updating the *bower.json* file. 
    * If you don't know how to use bower, put your dependency in a lib folder, and *raise a new issue for me*.

### Project Structure ###

This is a snapshot of the current project structure : 

```
.
├── bower_components - all the project dependencies
├── config - configuration files
├── css - contains all the stylesheets
├── fonts - special fonts
├── img - images , especially markers
└── js - all the javascript code
    ├── backbone - all the backbone related code
    │   ├── collections
    │   ├── models
    │   ├── templates
    │   └── views
    ├── maps - leaflet and google map custom libraries
    ├── utils - other javascript code. 
    └── vendor - should probably be removed 
```

Other important files : 

* bower.json - the file listing all dependencies for the project.
* index.html - currently the only html file of the project. 

### Third parties ###

The project currently uses the APIs from Google to display its data.

### Contribution guidelines ###

All contributions are very welcome!
For maximum efficiency, please try to follow the following guidelines : 

* **Create an issue** for anything you want to have / do / see. This avoids me having to refuse code because it doesn't match the idea of the project. Better discuss in the issue first.
* Any **Pull request should link to a precise issue**. Special kudos if you **name your branch with the issue number**.
* **Please do not work in master**. I want to test your code before merging it in master.

Feel free to modifiy / create / edit issues. **Issues are meant to be dynamic.** 
You are very welcome to point out bugs, problems and ideas.

Main point of contact for the project is [Jlengrand](https://bitbucket.org/jlengrand). 
Feel free to drop me a line.


*****

## Current development plan ##

![WhereShouldILive plan.jpg](https://bitbucket.org/repo/bA78LG/images/2549394398-WhereShouldILive%20plan.jpg)


The drawing displayed above is a rough mockup of what the first usable version of the app would look like.
*It is of course not a final design, but aims at expressing what are the next needs in terms of development*

###1.  General information ###

The application is separated into 2 main parts : 
* **The map**, where the info is shown to the user
* **The side panel**, that will contain everything to help him reach his goal.

The goal of the visitor is to find **where it is best for him to live**.
For that, we will take into account the things he finds most important.

###2.  Side panel description ###

####1.  Set up part ####

##### Map #####

First of all, he chooses in which city / region he will be searching for a new place. This is the objective of the City part, in light green.

####2. Data ####

Once the city is selected, the user has access to several types of data he can play around with.

##### General Locations #####

Those are main appliances. They basically are things like bars, hospitals, grocery stores,  . . .
The idea here is to let the user know in which areas are the appliances he is searching for concentrated.
In this part, the user has little choice on what to select or not. The choice is limited to a type of data.

##### Custom Locations #####

Custom locations are defined using addresses. It can be a restaurant that the user knows he likes, or the place he works, or the house of a family member for example. 
In this case, we should allow the user to set a marker by different ways (click on the map, address selection,  . . .).

It is possible for a user to select as many special locations as he wants.

##### Layers #####

Layers is a special type of data, that may not always be available.
It corresponds to general data about the area. This can be related to the criminality rate, the education level, the price per square meter,  . . .
Whatever can be important for the user.

####3. User Selection ####

The orange screen in the schema corresponds to the actual selection from the user. 
The current way to see it is some kind of dropdown of special selection he made from the data elements. 
He can drop as many elements as he wants in his selection, sorted by order of importance. 

Both general and custom locations should contain some sort of time travel element. It should be possible for the user to select a type of locomotion (foot, bike, car, train) and a maximum time of travel.
Ex : I want to live within 30 minutes of this address. 

Once his selection is made, the user should be able to finally reach the most important part of his journey : **the generation of the heatmap.**

####4. Generation of the Heatmap ####

The heatmap should be generated using all the elements selected by the user. 
The algorithm for this still has to be defined. 
Once the heatmap is calculated, it should be showed on the map, so that the user can easily see in which part of the city it is best for him to search for a place. 

In the long run, we can think about being able to directly place houses to sell in the area, based on the results from the heatmap.

*****

### Main contributors ###

So far, there has mainly been 3 people involved in the project

* Julien ([Jlengrand](https://bitbucket.org/jlengrand)), main contributor and developer
* Ramon Gebben ([RamonGebben](https://bitbucket.org/RamonGebben)), designer and creator of everything shiny / beautiful in the app!
* Jan Martijn ([Sandstorm01](https://bitbucket.org/Sandstorm01)), bringing ideas and new features!

**Special thanks to both of you for your help ;).**

*And here is a photo of a cat! (That I let just to please Ramon)*

![Cats](http://lorempixel.com/128/72/cats)

### Competition / Equivalents ###

There are a few things out there that look like what *WhereShouldILive* wants to achieve.
The list below can be used to get inspired : 

* [Trulia](http://www.trulia.com/local/new-york-ny/tiles:1%7Cpoints:1_crime), a website aiming at selling high-end houses.
* [Mapnificient](http://www.mapnificent.net/london/#/?lat=51.52444546903334&lng=-0.25294315966800696&zoom=12&lat0=51.4979344583005&lng0=-0.2092344025878674&t0=15), shows you areas you can reach with public transport in a given time in London. Excellent example of what custom locations results should look like.
* [Wikimapia](http://wikimapia.org/#lang=en&lat=51.924790&lon=6.310272&z=9&m=b&tag=27) Project aiming at describing the world.
* [http://www.walkscore.com/](WalkScore). We basically want to build that for Europe.

### Things to read ###

Interesting links I found that I have to read and can be useful for the project :).

* [Finding the perfect house using open data](http://dealloc.me/2014/05/24/opendata-house-hunting/?utm_content=buffere775a&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
* [Using the Leaflet.draw plugin for leaflet.js](http://www.d3noob.org/2014/01/using-leafletdraw-plugin-for-leafletjs.html)
* [Using OAuth 2.0 for Client-side Applications](https://developers.google.com/accounts/docs/OAuth2UserAgent)
* [Internationalization frameworks](http://stackoverflow.com/questions/9640630/javascript-i18n-internationalization-frameworks-libraries-for-client-side-use)
* [Route360 - Large Geographic Network Analysis - Route Planning - Visualization](http://developers.route360.net/)
* [Leaflet.awesome-markers](https://github.com/lvoogdt/Leaflet.awesome-markers)
* [A Comparison of Places APIs](http://blog.newsplore.com/2011/03/23/a-comparison-of-places-apis)
* [Yahoo Geoplanet](https://developer.yahoo.com/geo/geoplanet/)
* [skobbler - smart location-aware apps library](http://developer.skobbler.com/#tour)
* [GeoJson specifications](http://geojson.org/geojson-spec.html)

### License ###

This project is under **[Creative Commons Attribution NonCommercial NoDerivs (CC-NC-ND)
](https://tldrlegal.com/license/creative-commons-attribution-noncommercial-noderivs-(cc-nc-nd))**. 
See **[here](https://tldrlegal.com/license/creative-commons-attribution-noncommercial-noderivs-(cc-nc-nd)#fulltext)** for the full text.