require([
        '../js/backbone/models/location.js',
    ],
    function(Location){
        QUnit.test( "create location", function( assert ) {
        var testLocation = new Location({name: 'test location', latitude: '0.12', longitude:'021'});
        assert.equal("test location",testLocation.attributes.name);
    });
});