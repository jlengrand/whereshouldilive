/**
 * Created by jll on 21-7-14.
 */
/**
 * A LocationType is one of the types of points of interest a user can decide is important.
 * Examples are train stations, churches, . . .
 */
define([
    'backbone/collections/area_of_interest_list'
    ], function (AreaOfInterestList) {

    var LocationType = Backbone.Model.extend({

        defaults: {
            id: null,
            name: null,
            type: null,
            areaProperties: null,
            areaOfInterestList: null
        },

        initialize: function () {

            this.set('areaOfInterestList', new AreaOfInterestList());
        },
        hideAreas: function () {
            console.log("LocationType : Hiding all elements");
            this.get('areaOfInterestList').hide();
        }
    });

    return LocationType;
});