/**
 * Created by jlengrand on 14-8-14.
 */
/*
 A Custom Location is a map coordinate set by the user.
 It is based on a position, but represents more than that.
 */
define(['backbone/models/location'], function (Location) {
    var CustomLocation = Location.extend({
        defaults:{
            name: null
        },

        initialize: function(){
          console.log("New Custom Location created !");
        }
    });
    return CustomLocation;
});