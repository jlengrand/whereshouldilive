/**
 * Created by jll on 21-7-14.
 */

/**
 * Defines an area of interest.
 * Each result from a radar search is an AOI.
 * An AOI is currently defined by a marker, and a corresponding circle.
 * @type {*|void}
 */
define([], function () {

    var AreaOfInterest = Backbone.Model.extend({
        defaults: {
            marker: null, // center of the area
            area: null // area around the center
        },
        initialize: function () {
        }

    });

    return AreaOfInterest;
});