    /**
 * Created by jll on 16-8-14.
 */
/*
 Defines how the collection of custom locations look like
 */
 define(['text!backbone/templates/custom_locations.html'
        ], function (Template) {

    var CustomLocationsView = Backbone.View.extend({
        initialize: function(){
            this.collection.bind('add', this.render, this);
            this.render();
        },
        render: function(){
            var template = _.template(Template);
            this.$el.html(template({
                collection: this.collection.toJSON()
            }));
        }
    });
    return CustomLocationsView;

});