/**
 * Created by jll on 21-7-14.
 */

/**
 * Collection of AreaOfInterest. Each location type should have one.
 */
define([
    'backbone/models/area_of_interest',
    'maps/leafletMaps',
    'bower_components/leaflet.markercluster/dist/leaflet.markercluster.js'
    ], function (AreaOfInterest,
                 MapsLib){

    var AreaOfInterestList = Backbone.Collection.extend({
        model: AreaOfInterest,
        initialize: function () {
            this.markerCluster  = new L.MarkerClusterGroup();
        },
        /*
         Displays all Area Of Interests in the collection
         */
        display: function () {
            console.log("Displaying all Areas of Interest.");
            var self = this;

            //Renewing the cluster group to avoid having duplicates
            this.markerCluster  = new L.MarkerClusterGroup();

            this.each(function (aoi) {
                self.markerCluster.addLayer(aoi.get("marker"));
                MapsLib.getMapsLib().addElement(aoi.get("area"));
            });
            //Finally adding the clust of elements
            MapsLib.getMapsLib().addElement(self.markerCluster);
        },
        /*
         Hides all Area Of Interests in the collection
         */
        hide: function () {
            console.log("Hiding all Areas of Interest.");
            MapsLib.getMapsLib().hideElement(this.markerCluster);
            var self = this;
            this.each(function (aoi) {
                // MapsLib.getMapsLib().hideElement(aoi.get("marker"));
                MapsLib.getMapsLib().hideElement(aoi.get("area"));
            });
        }
    });

    return AreaOfInterestList;
});