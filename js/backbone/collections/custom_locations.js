/**
 * Created by jll on 16-8-14.
 */
/*
 * Collection of CustomLocations.
 */
define(['backbone/models/custom_location'], function (CustomLocation) {
    var CustomLocations = Backbone.Collection.extend({
        initialize: function() {
        },
        model: CustomLocation
    });

    return CustomLocations;
});