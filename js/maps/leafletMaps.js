/**
 * Created by jll on 25-7-14.
 */
define(['backbone/models/area_of_interest'], function (AreaOfInterest) {

    /**
     * Contains all the code related to leaflet API.
     * Handles everything displayed on the user interface
     * @constructor
     */
    var MapsLib = {

        map : new L.Map('map', {center: new L.LatLng(52.0802, 5.1028), zoom: 14}),
        init: function(){
            var googleLayer = new L.Google('ROADMAP');
            this.map.addLayer(googleLayer);
        },

        /**
         * Set the google map bounds to be the same as the leaflet one.
         */
//        this.setGoogleMapBounds = function(){
//
//            // Setting map bounds at startup
//            var bounds = geoLib.convertLeafletToGoogleBounds();
//            googleMap.fitBounds(bounds);
//
//            // Registering event to set google bounds everytime the leaflet map moves
//            map.on('moveend', function(e) {
//                var bounds = geoLib.convertLeafletToGoogleBounds();
//                // console.log("Fitting map to new bounds : " + bounds);
//                googleMap.fitBounds(bounds);
//            });
//            // TODO: We want a listener here to always be good. The bounds should always be the same.
//        }

        /**
        * Returns the current center of the map, as a LatLng object
        */
        getCenter: function(){
            return this.map.getCenter();
        },

        /**
         * Adds a new element to the map
         */
        addElement : function(element){
            this.map.addLayer(element);
        },

        /**
         * Hides an element from the map
         */
        hideElement : function(element){
            this.map.removeLayer(element);
        },

        /*
         Sets the current map coordinates to a new value, given in the interface
         */
        pan : function(lat, lng){
            //this.map.panTo(new L.LatLng(lat, lng));
            this.map.setView(new L.LatLng(lat, lng), 14, true);
        },

        createAreasOfInterest : function(item, radarResults) {
            var areaProperties = item.get('areaProperties');
            var areaOfInterestList = item.get('areaOfInterestList');
            for (var i = 0, result; result = radarResults[i]; i++) {
                //Create AOI and adds it to the item.
                // TODO: Avoid duplicates
                var aoi = this.createAreaOfInterest(result, areaProperties);
                areaOfInterestList.add(aoi);
            }
            item.set({areaOfInterestList: areaOfInterestList});

            var aoil = item.get('areaOfInterestList');
            aoil.display();
        },

        createAreaOfInterest : function(place, areaProperties) {
            // Creates marker to give info about the place.
            var marker = this.createMarker(place, areaProperties);
            //Create area of interest around the point
            var circle = this.createCircle(place, areaProperties);
            var aoi = new AreaOfInterest({ marker: marker, area: circle});

            return aoi;
        },

        createMarker : function(place, areaProperties) {
            var position = new L.latLng(place.geometry.location.lat(), place.geometry.location.lng());
            var marker = new L.marker( position, {icon: areaProperties.get('icon')} );

            return marker;
        },

        createCircle : function(place, areaProperties) {
            var pos = new L.latLng(place.geometry.location.lat(), place.geometry.location.lng());
            var circle = new L.circle(
                pos,
                500, // 500 meters for now
                {
                    color: areaProperties.get('strokeColor'),
                    opacity: areaProperties.get('strokeOpacity'),
                    weight: areaProperties.get('strokeWeight'),
                    fillColor: areaProperties.get('fillColor'),
                    fillOpacity: areaProperties.get('fillOpacity')
                })

            return circle;
        }

    };
    return {
        getMapsLib: function () { return MapsLib; }
    };
});