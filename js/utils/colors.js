/**
 * Created by jll on 21-7-14.
 */

/*
 List of colors. Avoids having to directly use hex values
 FIXME: Do that better
 */
define([], function () {

    var Colors = function() {
        this.blue = '#0074d9';
        this.aqua = '#7fdbff';
        this.teal = '#39cccc';
        this.olive = '#3d9970';
        this.green = '#2ecc40';
        this.lime = '#01ff70';
        this.yellow = '#ffdc00';
        this.orange = '#ff851b';
        this.red = '#ff4136';
        this.maroon = '#85144b';
        this.fuchsia = '#f012be';
        this.purple = '#b10dc9';
        this.white = '#ffffff';
        this.silver = '#dddddd';
        this.gray = '#aaaaaa';
        this.black = '#111111';
    };

    return Colors;
});
