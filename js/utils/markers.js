

/*
 List of markers. Defines specialized icons for display in leaflet.
 */
define([], function () {
    var Markers = function() {
        var WhereDoILiveIcon = L.Icon.extend({
            options: {
                iconSize:     [48, 48], // size of the icon
                iconAnchor:   [24, 38], // point of the icon which will correspond to marker's location
                popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
            }
        });

        this.airport = new WhereDoILiveIcon({iconUrl: '/img/marker/airport-128.png'});
        this.drug_store = new WhereDoILiveIcon({iconUrl: '/img/marker/drug_store-128.png'});
        this.hospital = new WhereDoILiveIcon({iconUrl: '/img/marker/hospital-128.png'});
        this.post_office = new WhereDoILiveIcon({iconUrl: '/img/marker/post_office-128.png'});
        this.shopping_center = new WhereDoILiveIcon({iconUrl: '/img/marker/shopping_center-128.png'});
        this.university_school = new WhereDoILiveIcon({iconUrl: '/img/marker/university_school-128.png'});
        this.bank_atm = new WhereDoILiveIcon({iconUrl: '/img/marker/bank_atm-128.png'});
        this.first_help = new WhereDoILiveIcon({iconUrl: '/img/marker/first_help-128.png'});
        this.hotel = new WhereDoILiveIcon({iconUrl: '/img/marker/hotel-128.png'});
        this.pub_beer = new WhereDoILiveIcon({iconUrl: '/img/marker/pub_beer-128.png'});
        this.special_location = new WhereDoILiveIcon({iconUrl: '/img/marker/special_location-128.png'});
        this.bar_lounge = new WhereDoILiveIcon({iconUrl: '/img/marker/bar_lounge-128.png'});
        this.fitness_center = new WhereDoILiveIcon({iconUrl: '/img/marker/fitness_center-128.png'});
        this.more_location = new WhereDoILiveIcon({iconUrl: '/img/marker/more_location-128.png'});
        this.rail_station = new WhereDoILiveIcon({iconUrl: '/img/marker/rail_station-128.png'});
        this.theatre = new WhereDoILiveIcon({iconUrl: '/img/marker/theatre-128.png'});
        this.cafe = new WhereDoILiveIcon({iconUrl: '/img/marker/cafe-128.png'});
        this.gas_oil_station = new WhereDoILiveIcon({iconUrl: '/img/marker/gas_oil_station-128.png'});
        this.museum = new WhereDoILiveIcon({iconUrl: '/img/marker/museum-128.png'});
        this.restaurant = new WhereDoILiveIcon({iconUrl: '/img/marker/restaurant-128.png'});
        this.tram = new WhereDoILiveIcon({iconUrl: '/img/marker/tram-128.png'});
        this.church = new WhereDoILiveIcon({iconUrl: '/img/marker/church-128.png'});
        this.home = new WhereDoILiveIcon({iconUrl: '/img/marker/home-128.png'});
        this.parking = new WhereDoILiveIcon({iconUrl: '/img/marker/parking-128.png'});
        this.seaport = new WhereDoILiveIcon({iconUrl: '/img/marker/seaport-128.png'});
        this.travel_agency = new WhereDoILiveIcon({iconUrl: '/img/marker/travel_agency-128.png'});
    };
    return Markers;
});
